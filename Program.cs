﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        private static int REINITIALIZE_TICKS = 1000;

        private IMyBroadcastListener listener;

        private List<TimeDisplay> displays = new List<TimeDisplay>();

        private int ticks = 0;

        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update100;
            initialize();
        }

        public void initialize()
        {
            var configuration = new BaseConfiguration(Me);
            listener = IGC.RegisterBroadcastListener(configuration.getChannel());
            listener.SetMessageCallback(configuration.getChannel());

            displays.Clear();

            var blocks = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType(blocks, b => b is IMyTextSurfaceProvider && TimeDisplayConfiguration.hasConfiguration(b));

            blocks.ForEach(delegate (IMyTerminalBlock block)
            {
                addBlock(block);
            });
        }

        private void addBlock(IMyTerminalBlock block)
        {
            var configuration = new TimeDisplayConfiguration(block);
            var display = ((IMyTextSurfaceProvider)block).GetSurface(configuration.getDisplay());

            Echo("adding display " + block.CustomName + " with display " + configuration.getDisplay() + ", format " + configuration.getFormat() + " and timezone offset " + configuration.getTimezoneOffset());

            displays.Add(new TimeDisplay(configuration.getTimezoneOffset(), configuration.getFormat(), display));
        }

        public void Main(string argument, UpdateType updateSource)
        {
            if (updateSource == UpdateType.Terminal)
            {
                if (argument.Count() == 0)
                {
                    initialize();
                }
                else
                {
                    var targetBlock = GridTerminalSystem.GetBlockWithName(argument);
                    if (targetBlock == null)
                    {
                        Echo("could not find block " + argument);
                    }
                    else if (!(targetBlock is IMyTextSurfaceProvider))
                    {
                        Echo("block " + argument + " does not have displays");
                    }
                    else
                    {
                        addBlock(targetBlock);
                    }
                }
            }
            else if (updateSource == UpdateType.Update100 && ticks++ > REINITIALIZE_TICKS)
            {
                ticks = 0;
                initialize();
            }
            else
            {
                while (listener.HasPendingMessage)
                {
                    var ingameMilliseconds = (long)listener.AcceptMessage().Data;
                    DateTime date = DateObject.toDateTime(ingameMilliseconds);
                    Echo("current datetime: " + date);
                    displays.ForEach(delegate (TimeDisplay display)
                    {
                        display.update(date);
                    });
                }
            }
        }

        private class TimeDisplay
        {
            private int timezoneOffset;
            private List<string> format;
            private IMyTextSurface surface;

            public TimeDisplay(int timezoneOffset, List<string> format, IMyTextSurface surface)
            {
                this.timezoneOffset = timezoneOffset;
                this.format = format;
                this.surface = surface;
            }

            public void update(DateTime dateTime)
            {
                dateTime = dateTime.AddHours(timezoneOffset);
                surface.WriteText("");
                format.ForEach(delegate (string line)
                {
                    surface.WriteText(String.Format(line, dateTime)+"\n", true);
                });
            }
        }
    }
}
