﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    class TimeDisplayConfiguration
    {
        private static string DEFAULT_FORMAT = "{0:dd.MM.yyyy HH:mm}";
        private static int DEFAULT_DISPLAY = 0;
        private static int DEFAULT_TIMEZONE_OFFSET = 0;
        protected static string SECTION = "INTP Display Configuration";
        private static string FORMAT_KEY = "format";
        private static string DISPLAY_KEY = "display";
        private static string TIMEZONE_OFFSET_KEY = "timezone";

        protected MyIni ini;

        private IMyTerminalBlock block;

        public static bool hasConfiguration(IMyTerminalBlock block)
        {
            var ini = new MyIni();
            ini.TryParse(block.CustomData);
            return hasConfiguration(ini);
        }

        private static bool hasConfiguration(MyIni ini)
        {
            return ini.ContainsSection(SECTION);
        }

        public TimeDisplayConfiguration(IMyTerminalBlock block)
        {
            this.block = block;
            ini = new MyIni();
            ini.TryParse(block.CustomData);

            if (!hasConfiguration(ini))
            {
                createDefaultConfiguration();
            }
        }

        protected virtual Dictionary<string, object> defaultConfiguration()
        {
            Dictionary<string, object> defaults = new Dictionary<string, object>();
            defaults.Add(FORMAT_KEY, DEFAULT_FORMAT);
            defaults.Add(DISPLAY_KEY, DEFAULT_DISPLAY);
            defaults.Add(TIMEZONE_OFFSET_KEY, DEFAULT_TIMEZONE_OFFSET);
            return defaults;
        }

        private void createDefaultConfiguration()
        {
            ini.AddSection(SECTION);
            var defaults = defaultConfiguration();
            defaults.Keys.ToList().ForEach(delegate (string key)
            {
                ini.Set(SECTION, key, defaults[key].ToString());
            });
            block.CustomData = ini.ToString();
        }

        public List<string> getFormat()
        {
            var lines = new List<string>();
            ini.Get(SECTION, FORMAT_KEY).GetLines(lines);
            return lines;
        }

        public int getDisplay()
        {
            return ini.Get(SECTION, DISPLAY_KEY).ToInt32();
        }

        public int getTimezoneOffset()
        {
            return ini.Get(SECTION, TIMEZONE_OFFSET_KEY).ToInt32();
        }
    }
}
